const express = require('express')

const app = express()

app.get('/', (response, request) => {
  request.send('Welcome here')
  console.log('Successfull')
})

app.listen(4000, '0.0.0.0', () => {
  console.log('Server started')
  process.exit()
})
